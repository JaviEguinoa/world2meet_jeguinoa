# BaseImage with java 17
FROM adoptopenjdk/openjdk17:alpine-jre

# Se copia el jar generado
WORKDIR /
COPY build/libs/spacecraft.jar spacecraft.jar

EXPOSE 8080
CMD ["java", "-jar", "spacecraft.jar"]