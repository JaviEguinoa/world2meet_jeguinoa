package com.hiberus.world2meet.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.coyote.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hiberus.world2meet.exchangeModel.SpacecraftExchange;
import com.hiberus.world2meet.service.SpacecraftService;
import com.hiberus.world2meet.webservice.SpacecraftApi;

@WebMvcTest(SpacecraftApi.class)
public class SpacecraftApiTest {

	@Autowired
	private MockMvc mockMvc;

	SpacecraftExchange exchangeWithId1 = new SpacecraftExchange(1, "Halcón milenario", "Es la nave de Han solo.",
			"Star Wars");

	SpacecraftExchange exchangeWithNoId = new SpacecraftExchange(null, "Halcón milenario", "Es la nave de Han solo.",
			"Star Wars");
	
	@MockBean
	private SpacecraftService spacecraftService;

	@BeforeEach
	public void initTest() {
		// mockeamos algunas de las llamadas más comunes en nuestros tests
		when(this.spacecraftService.findSpacecraftById(1)).thenReturn(exchangeWithId1);
		when(this.spacecraftService.findSpacecraftById(4)).thenReturn(null);
		
		when(this.spacecraftService.deleteSpacecraft(1)).thenReturn(true);
		when(this.spacecraftService.deleteSpacecraft(4)).thenReturn(false);
		
		try {
			when(this.spacecraftService.createSpacecraft(exchangeWithId1)).thenThrow(new BadRequestException("elemento con id no nulo"));
			when(this.spacecraftService.createSpacecraft(exchangeWithNoId)).thenReturn(exchangeWithId1);
			
			when(this.spacecraftService.updateSpacecraft(exchangeWithNoId)).thenThrow(new BadRequestException("elemento con id nulo"));
			when(this.spacecraftService.updateSpacecraft(exchangeWithId1)).thenReturn(exchangeWithId1);
		} catch (BadRequestException e) {
			e.printStackTrace();
		}
	}

	@Test
	void testGetSpacecraftExists() throws Exception {
		mockMvc.perform(get("/wsAPIv1/spacecraft/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id").value(1L))
				.andExpect(jsonPath("$.name").value("Halcón milenario"))
				.andExpect(jsonPath("$.description").value("Es la nave de Han solo."))
				.andExpect(jsonPath("$.film").value("Star Wars"));
	}

	@Test
	void testGetSpacecraftNotExists() throws Exception {
		mockMvc.perform(get("/wsAPIv1/spacecraft/4")).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorMessage").value("No se ha encontrado la nave con id 4"));
	}

	@Test
	void testDeleteSpacecraftExists() throws Exception {
		mockMvc.perform(delete("/wsAPIv1/spacecraft/1")).andExpect(status().isOk());
	}

	@Test
	void testDeleteSpacecraftNotExists() throws Exception {
		mockMvc.perform(delete("/wsAPIv1/spacecraft/4")).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorMessage").value("No existe la nave con id 4"));
	}
	
	@Test
	void testCreateSpacecraftWithNoId() throws JsonProcessingException, Exception {
		mockMvc.perform(post("/wsAPIv1/spacecraft")
				.contentType(MediaType.APPLICATION_JSON)
				.content((new ObjectMapper()).writeValueAsString(exchangeWithNoId)))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.id").value(1L))
				.andExpect(jsonPath("$.name").value("Halcón milenario"))
				.andExpect(jsonPath("$.description").value("Es la nave de Han solo."))
				.andExpect(jsonPath("$.film").value("Star Wars"));
	}

	@Test
	void testCreateSpacecraftWithId() throws JsonProcessingException, Exception {
		mockMvc.perform(post("/wsAPIv1/spacecraft")
				.contentType(MediaType.APPLICATION_JSON)
				.content((new ObjectMapper()).writeValueAsString(exchangeWithId1)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("elemento con id no nulo"));
	}
	
	@Test
	void testUpdateSpacecraftWithId() throws JsonProcessingException, Exception {
		mockMvc.perform(put("/wsAPIv1/spacecraft")
				.contentType(MediaType.APPLICATION_JSON)
				.content((new ObjectMapper()).writeValueAsString(exchangeWithId1)))
				.andExpect(status().isAccepted()).andExpect(jsonPath("$.id").value(1L))
				.andExpect(jsonPath("$.name").value("Halcón milenario"))
				.andExpect(jsonPath("$.description").value("Es la nave de Han solo."))
				.andExpect(jsonPath("$.film").value("Star Wars"));
	}

	@Test
	void testUpdateSpacecraftWithNoId() throws JsonProcessingException, Exception {
		mockMvc.perform(put("/wsAPIv1/spacecraft")
				.contentType(MediaType.APPLICATION_JSON)
				.content((new ObjectMapper()).writeValueAsString(exchangeWithNoId)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("elemento con id nulo"));
	}
}
