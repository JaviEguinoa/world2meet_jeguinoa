package com.hiberus.world2meet.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.coyote.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.hiberus.world2meet.World2meetApplication;
import com.hiberus.world2meet.exchangeModel.SpacecraftExchange;
import com.hiberus.world2meet.model.SpacecraftModel;
import com.hiberus.world2meet.repository.SpacecraftRepository;

@SpringBootTest(classes = World2meetApplication.class)
public class SpacecraftServiceTest {

    SpacecraftModel modelWithId1 = new SpacecraftModel(1, "Halcón milenario", "Es la nave de Han solo.", "Star Wars");
    SpacecraftModel modelWithId2 = new SpacecraftModel(2, "USS Enterprise", "Es la nave de star trek", "Star Trek");
    SpacecraftModel modelWithId3 = new SpacecraftModel(3, "Nostromo", "Nave de Alien el octavo pasajero", "Alien");

    SpacecraftExchange exchangeWithId1 = new SpacecraftExchange(1, "Halcón milenario", "Es la nave de Han solo.", "Star Wars");
    SpacecraftExchange exchangeWithId2 = new SpacecraftExchange(2, "USS Enterprise", "Es la nave de star trek", "Star Trek");
    SpacecraftExchange exchangeWithId3 = new SpacecraftExchange(3, "Nostromo", "Nave de Alien el octavo pasajero", "Alien");

    @Mock
    private SpacecraftRepository spacecraftRepository;

    private SpacecraftService spacecraftService;
    
    @BeforeEach
    public void initTest() {
        //mockeamos algunas de las llamadas más comunes en nuestros tests
        SpacecraftModel spacecraft = this.modelWithId1;
        when(this.spacecraftRepository.findById(1)).thenReturn(Optional.of(spacecraft));
        when(this.spacecraftRepository.findById(4)).thenReturn(Optional.empty());

        this.spacecraftService = new SpacecraftService(spacecraftRepository);
    }

    //Testeo de cada uno de los métodos del webservice
    //

    @Test
    public void checkIfExistWhithExistingSpacecraft() {
        Integer parameter = 1;
        SpacecraftExchange exchange = this.spacecraftService.findSpacecraftById(parameter);
        assertNotNull(exchange);
        assertEquals(parameter, exchange.getId());
        assertEquals("Star Wars", exchange.getFilm());
        assertEquals("Halcón milenario", exchange.getName());
        assertEquals("Es la nave de Han solo.", exchange.getDescription());
    }
    @Test
    public void checkIfExistWhithNotExistingSpacecraft() {
        Integer parameter = 4;
        SpacecraftExchange exchange = this.spacecraftService.findSpacecraftById(parameter);
        assertNull(exchange);
    }
    @Test
    public void checkFindSpacecraftByQueryExistingWildcard() {
        String wildcard = "prueba";
        Pageable firstPage = PageRequest.of(0, 2);
            when(this.spacecraftRepository.findByNameLikeOrderByName(wildcard, firstPage))
                    .thenReturn(Arrays.asList(this.modelWithId1, this.modelWithId2));
        Pageable secondPage = PageRequest.of(1, 2);
        when(this.spacecraftRepository.findByNameLikeOrderByName(wildcard, secondPage))
                .thenReturn(Arrays.asList(this.modelWithId3));

        this.spacecraftService = new SpacecraftService(spacecraftRepository);
        
        List<SpacecraftExchange> result = this.spacecraftService.findSpacecraftByQuery(wildcard, firstPage);

        assertTrue(!result.isEmpty() && result.size() == 2);
        List<Integer> ids = result.stream().map(SpacecraftExchange::getId).collect(Collectors.toList());
        assertTrue(ids.contains(this.modelWithId1.getId()) && ids.contains(this.modelWithId2.getId()));

        List<String> stringsToSearch = result.stream().map(SpacecraftExchange::getName).collect(Collectors.toList());
        assertTrue(stringsToSearch.contains(this.modelWithId1.getName()) && stringsToSearch.contains(this.modelWithId2.getName()));

        stringsToSearch = result.stream().map(SpacecraftExchange::getFilm).collect(Collectors.toList());
        assertTrue(stringsToSearch.contains(this.modelWithId1.getFilm()) && stringsToSearch.contains(this.modelWithId2.getFilm()));

        stringsToSearch = result.stream().map(SpacecraftExchange::getDescription).collect(Collectors.toList());
        assertTrue(stringsToSearch.contains(this.modelWithId1.getDescription()) && stringsToSearch.contains(this.modelWithId2.getDescription()));

        result = this.spacecraftService.findSpacecraftByQuery(wildcard, secondPage);

        assertTrue(!result.isEmpty() && result.size() == 1);
        ids = result.stream().map(SpacecraftExchange::getId).collect(Collectors.toList());
        assertTrue(ids.contains(this.modelWithId3.getId()));

        stringsToSearch = result.stream().map(SpacecraftExchange::getName).collect(Collectors.toList());
        assertTrue(stringsToSearch.contains(this.modelWithId3.getName()));

        stringsToSearch = result.stream().map(SpacecraftExchange::getFilm).collect(Collectors.toList());
        assertTrue(stringsToSearch.contains(this.modelWithId3.getFilm()));

        stringsToSearch = result.stream().map(SpacecraftExchange::getDescription).collect(Collectors.toList());
        assertTrue(stringsToSearch.contains(this.modelWithId3.getDescription()));
    }
    @Test
    public void checkFindSpacecraftByQueryNotExistingWildcard() {
        String wildcard = "prueba";
        Pageable firstPage = PageRequest.of(0, 2);
        when(this.spacecraftRepository.findByNameLikeOrderByName(wildcard, firstPage))
                .thenReturn(new ArrayList<>());

        List<SpacecraftExchange> result = this.spacecraftService.findSpacecraftByQuery(wildcard, firstPage);

        assertTrue(result.isEmpty());
    }

    @Test
    public void checkDeleteSpacecraftNotExistingId() {
        boolean result = this.spacecraftService.deleteSpacecraft(4);
        assertTrue(!result);
    }

    @Test
    public void checkDeleteSpacecraftExistingId() {
        boolean result = this.spacecraftService.deleteSpacecraft(1);
        assertTrue(result);
    }

    @Test
    public void checkCreateSpacecraftExchangeWithId() {
        SpacecraftExchange exchangeWithId = new SpacecraftExchange(4,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        BadRequestException exception = assertThrows(BadRequestException.class, () -> {this.spacecraftService.createSpacecraft(exchangeWithId);});

        assertEquals("elemento con id no nulo", exception.getMessage());
    }

    @Test
    public void checkUpdateSpacecraftExchangeWithNoId() throws BadRequestException {
        SpacecraftExchange exchange = new SpacecraftExchange(null,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        SpacecraftModel model = new SpacecraftModel(null,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        SpacecraftModel modelWithId = new SpacecraftModel(5,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        when(this.spacecraftRepository.save(model)).thenReturn(modelWithId);
        
        this.spacecraftService = new SpacecraftService(spacecraftRepository);
        
        SpacecraftExchange result = this.spacecraftService.createSpacecraft(exchange);

        assertEquals(5, result.getId());
        assertEquals(result.getName(), modelWithId.getName());
        assertEquals(result.getDescription(), modelWithId.getDescription());
        assertEquals(result.getFilm(), modelWithId.getFilm());

    }

    @Test
    public void checkUpdateSpacecraftExchangeWithId() {
        SpacecraftExchange exchangeWithNoId = new SpacecraftExchange(null,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        BadRequestException exception = assertThrows(BadRequestException.class, () -> {this.spacecraftService.updateSpacecraft(exchangeWithNoId);});

        assertEquals("elemento con id nulo", exception.getMessage());
    }

    @Test
    public void checkCreateSpacecraftExchangeWithNoId() throws BadRequestException {
        SpacecraftExchange exchange = new SpacecraftExchange(5,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        SpacecraftModel modelWithId = new SpacecraftModel(5,"Fénix", "Nave de una serie cuyo nombre no recuerdo", "Galáctica");
        when(this.spacecraftRepository.save(modelWithId)).thenReturn(modelWithId);

        SpacecraftExchange result = this.spacecraftService.updateSpacecraft(exchange);

        assertEquals(result.getId(), modelWithId.getId());
        assertEquals(result.getName(), modelWithId.getName());
        assertEquals(result.getDescription(), modelWithId.getDescription());
        assertEquals(result.getFilm(), modelWithId.getFilm());

    }
}
