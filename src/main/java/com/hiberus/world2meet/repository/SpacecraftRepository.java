package com.hiberus.world2meet.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hiberus.world2meet.model.SpacecraftModel;

public interface SpacecraftRepository  extends JpaRepository<SpacecraftModel, Integer> {

    List<SpacecraftModel> findByNameLikeOrderByName(String query, Pageable pageable);

}
