package com.hiberus.world2meet.webservice;

import java.util.List;

import org.apache.coyote.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hiberus.world2meet.exception.BadObjectRequestException;
import com.hiberus.world2meet.exception.NoSpacecraftFoundException;
import com.hiberus.world2meet.exchangeModel.SpacecraftExchange;
import com.hiberus.world2meet.service.SpacecraftService;

@RestController
public class SpacecraftApi {

	private Logger apiLogger = LoggerFactory.getLogger(SpacecraftApi.class);
	
    private final SpacecraftService spacecraftService;

    public SpacecraftApi(SpacecraftService spacecraftService) {
        this.spacecraftService = spacecraftService;
    }

    @GetMapping("/wsAPIv1/spacecraft/{idSpacecraft}")
    public ResponseEntity<SpacecraftExchange> findSpacecraftById(@PathVariable("idSpacecraft") Integer idSpacecraft) {
    	this.apiLogger.info("Solicitada la nave con id {}", idSpacecraft);
    	
    	SpacecraftExchange result = this.spacecraftService.findSpacecraftById(idSpacecraft);
    	if (result == null) {
    		throw new NoSpacecraftFoundException("No se ha encontrado la nave con id " + idSpacecraft);
    	}
        return ResponseEntity.ok(result);
    }

    @GetMapping("/wsAPIv1/spacecraft")
    public ResponseEntity<List<SpacecraftExchange>> findSpacecraftByQuery(@RequestParam String filter, Pageable pageable) {
    	
    	if (pageable.getPageSize() > 100) {
    		throw new BadObjectRequestException("La página no puede ser superior a 100 naves");
    	}
        return ResponseEntity.ok(this.spacecraftService.findSpacecraftByQuery(filter, pageable));
    }

    @DeleteMapping("/wsAPIv1/spacecraft/{idSpacecraft}")
    public ResponseEntity<Void> deleteSpacecraft(@PathVariable("idSpacecraft") Integer idSpacecraft) {
    	this.apiLogger.info("Petición para eliminar la nave con id {}", idSpacecraft);
    	
        boolean deleted = this.spacecraftService.deleteSpacecraft(idSpacecraft);
        if (!deleted) {
        	throw new NoSpacecraftFoundException("No existe la nave con id " + idSpacecraft);
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/wsAPIv1/spacecraft")
    public ResponseEntity<SpacecraftExchange> createSpacecraft(@RequestBody SpacecraftExchange exchange) {
    	try {
    	
    		SpacecraftExchange result = this.spacecraftService.createSpacecraft(exchange);
    		return ResponseEntity.status(HttpStatus.CREATED).body(result);
    		
    	} catch(BadRequestException ex) {
    		throw new BadObjectRequestException(ex.getMessage());
    	}

    }

    @PutMapping("/wsAPIv1/spacecraft")
    public ResponseEntity<SpacecraftExchange> updateSpacecraft(@RequestBody SpacecraftExchange exchange) throws BadRequestException {
    	this.apiLogger.info("Petición para actualiza la nave con id {}", exchange.getId());
    	
    	try {
        	SpacecraftExchange result = this.spacecraftService.updateSpacecraft(exchange);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(result);    		
    	} catch(BadRequestException ex) {
    		throw new BadObjectRequestException(ex.getMessage());
    	}
    }
}
