package com.hiberus.world2meet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CentralizedExceptionHandler {

	@ExceptionHandler(NoSpacecraftFoundException.class)
    public ResponseEntity<ErrorObject> handleResourceNotFoundException(NoSpacecraftFoundException ex) {
		ErrorObject errorResponse = new ErrorObject(ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadObjectRequestException.class)
    public ResponseEntity<ErrorObject> handleInvalidInputException(BadObjectRequestException ex) {
    	ErrorObject errorResponse = new ErrorObject(ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
	
}
