package com.hiberus.world2meet.exception;

public class BadObjectRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6448813979266164556L;

	public BadObjectRequestException(String message) {
		super(message);
	}
	
}
