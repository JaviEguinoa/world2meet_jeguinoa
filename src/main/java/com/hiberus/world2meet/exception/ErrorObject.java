package com.hiberus.world2meet.exception;

import java.util.Date;

public class ErrorObject {

	private String errorMessage;
    private Date timestamp;

    public ErrorObject(String errorMessage) {
        this.errorMessage = errorMessage;
        this.timestamp = new Date();
    }

	public String getErrorMessage() { return errorMessage; }

	public void setErrorMessage(String errorMessage) { this.errorMessage = errorMessage; }

	public Date getTimestamp() { return timestamp; }

	public void setTimestamp(Date timestamp) { this.timestamp = timestamp; }

}
