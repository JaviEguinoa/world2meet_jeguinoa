package com.hiberus.world2meet.exception;

public class NoSpacecraftFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3469747553656174042L;

	public NoSpacecraftFoundException(String message) {
		super(message);
	}
	
}
