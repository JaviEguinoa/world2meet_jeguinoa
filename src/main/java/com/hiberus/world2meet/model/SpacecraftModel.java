package com.hiberus.world2meet.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "spacecraft")
public class SpacecraftModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "film")
    private String film;

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getFilm() { return film; }

    public void setFilm(String film) { this.film = film; }

    public SpacecraftModel(Integer id, String name, String description, String film) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.film = film;
    }

    public SpacecraftModel() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpacecraftModel spacecraft = (SpacecraftModel) o;

        if ((id == null && spacecraft.id != null) || (spacecraft.id == null && id != null)) return false;
        
        return (id == spacecraft.id) || (name == null ? spacecraft.name == null : name.equals(spacecraft.name)) &&
                   (description == null ? spacecraft.description == null : description.equals(spacecraft.description)) &&
                   (film == null ? spacecraft.film == null : film.equals(spacecraft.film));
    }
}
