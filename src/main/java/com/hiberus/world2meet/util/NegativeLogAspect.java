package com.hiberus.world2meet.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class NegativeLogAspect {

    private Logger aspectLogger = LoggerFactory.getLogger(NegativeLogAspect.class);

    @Before("execution(* com.hiberus.world2meet.webservice.SpacecraftApi.findSpacecraftById(..)) && args(idSpacecraft,..)")
    public void negativeIfLogIdIsNull(JoinPoint joinPoint, Integer idSpacecraft) {
        if (idSpacecraft < 0) {
            this.aspectLogger.warn("La llamada a la api se ha realizado con un ID negativo: {0}", idSpacecraft);
        }
    }
}
