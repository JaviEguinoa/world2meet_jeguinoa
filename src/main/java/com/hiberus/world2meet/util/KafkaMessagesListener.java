package com.hiberus.world2meet.util;

import org.apache.coyote.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hiberus.world2meet.exchangeModel.SpacecraftExchange;
import com.hiberus.world2meet.service.SpacecraftService;

@Component
public class KafkaMessagesListener {

	private Logger kafkaListenerLogger = LoggerFactory.getLogger(NegativeLogAspect.class);
	
	@Autowired
	private SpacecraftService spacecraftService;
	
	@KafkaListener(topics = "spacecraftTopic",
	        autoStartup = "${listen.auto.start:false}")
	private void kafkaMessageListener(String kafkaData) {
		SpacecraftExchange exchangeModel = this.isValidSpacecraftObject(kafkaData);
		if (exchangeModel != null) {	//Es un objeto válido
			try {
				this.spacecraftService.createSpacecraft(exchangeModel);
			} catch (BadRequestException e) {
				e.printStackTrace();
				this.kafkaListenerLogger.error("No es una nave válida");
			}
		} else {
			this.kafkaListenerLogger.error("El objeto recibido no es una nave");
		}
		
	}
	
	private SpacecraftExchange isValidSpacecraftObject(String spacecraftString) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(spacecraftString, SpacecraftExchange.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
