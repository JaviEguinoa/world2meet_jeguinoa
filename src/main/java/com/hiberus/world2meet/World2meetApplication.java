package com.hiberus.world2meet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableCaching
@SpringBootApplication
@EnableAspectJAutoProxy
public class World2meetApplication {

	public static void main(String[] args) {
		SpringApplication.run(World2meetApplication.class, args);
	}

}
