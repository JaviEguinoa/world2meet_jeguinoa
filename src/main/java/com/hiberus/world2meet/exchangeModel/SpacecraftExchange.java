package com.hiberus.world2meet.exchangeModel;

import java.io.Serializable;

public class SpacecraftExchange implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8091182543523645103L;
	private Integer id;
    private String name;
    private String description;
    private String film;

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getFilm() { return film; }

    public void setFilm(String film) { this.film = film; }

    public SpacecraftExchange(Integer id, String name, String description, String film) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.film = film;
    }

	public SpacecraftExchange() {
		super();
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpacecraftExchange spacecraft = (SpacecraftExchange) o;

        if ((id == null && spacecraft.id != null) || (spacecraft.id == null && id != null)) return false;
        
        return (id == spacecraft.id) || (name == null ? spacecraft.name == null : name.equals(spacecraft.name)) &&
                   (description == null ? spacecraft.description == null : description.equals(spacecraft.description)) &&
                   (film == null ? spacecraft.film == null : film.equals(spacecraft.film));
    }
}
