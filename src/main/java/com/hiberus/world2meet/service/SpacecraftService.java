package com.hiberus.world2meet.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.coyote.BadRequestException;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hiberus.world2meet.exchangeModel.SpacecraftExchange;
import com.hiberus.world2meet.model.SpacecraftModel;
import com.hiberus.world2meet.repository.SpacecraftRepository;

@Service
public class SpacecraftService {

    private final SpacecraftRepository spacecraftRepository;

    public SpacecraftService(SpacecraftRepository spacecraftRepository) {
        this.spacecraftRepository = spacecraftRepository;
    }

    @Cacheable(value = "spacecrafts", key = "#idSpacecraft", condition="#idSpacecraft!=null")
    public SpacecraftExchange findSpacecraftById(Integer idSpacecraft) {

        Optional<SpacecraftModel> optionalSpacecraft = this.spacecraftRepository.findById(idSpacecraft);

        return optionalSpacecraft.map(this::castModelToExchange).orElse(null);
    }

    public List<SpacecraftExchange> findSpacecraftByQuery(String filter, Pageable pageable) {
        return this.spacecraftRepository.findByNameLikeOrderByName(filter, pageable).stream()
                .map(this::castModelToExchange).collect(Collectors.toList());
    }

    @CacheEvict(value = "spacecrafts", key = "#idSpacecraft", condition="#idSpacecraft!=null")
    public boolean deleteSpacecraft(Integer idSpacecraft) {
        if (this.spacecraftRepository.findById(idSpacecraft).isPresent()) {
            this.spacecraftRepository.deleteById(idSpacecraft);
            return true;
        } else {
            return false;
        }
    }

    public SpacecraftExchange createSpacecraft(SpacecraftExchange spacecraft) throws BadRequestException {
        if (spacecraft.getId() != null) {
            throw new BadRequestException("elemento con id no nulo");
        }
        SpacecraftModel entity = this.castExchangeToModel(spacecraft);
        entity = this.spacecraftRepository.save(entity);

        return this.castModelToExchange(entity);
    }

    @CachePut(value = "spacecrafts", key = "#spacecraft.id", condition="#idSpacecraft!=null")
    public SpacecraftExchange updateSpacecraft(SpacecraftExchange spacecraft) throws BadRequestException {
        if (spacecraft.getId() == null) {
            throw new BadRequestException("elemento con id nulo");
        }
        SpacecraftModel entity = this.castExchangeToModel(spacecraft);
        this.spacecraftRepository.save(entity);

        return this.castModelToExchange(entity);
    }

    private SpacecraftExchange castModelToExchange(SpacecraftModel entity) {
        return new SpacecraftExchange(entity.getId(), entity.getName(), entity.getDescription(), entity.getFilm());
    }

    private SpacecraftModel castExchangeToModel(SpacecraftExchange exchangeModel) {
        return new SpacecraftModel(exchangeModel.getId(), exchangeModel.getName(), exchangeModel.getDescription(), exchangeModel.getFilm());
    }
}
