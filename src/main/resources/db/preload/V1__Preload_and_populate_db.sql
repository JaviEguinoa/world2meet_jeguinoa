CREATE TABLE spacecraft (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(255) NOT NULL,
    film VARCHAR(100) NOT NULL
);

INSERT INTO spacecraft (name, description, film) VALUES
('Halcón milenario', 'Es la nave de Han solo.', 'Star Wars'),
('USS Enterprise', 'Es la nave de star trek', 'Star Trek'),
('Nostromo', 'Nave de Alien el octavo pasajero', 'Alien'),
('Fénix', 'Nave de una serie cuyo nombre no recuerdo', 'Galáctica');