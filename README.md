# world2meet_jeguinoa



## Getting started

Se adjunta la aplicación. Los puntos opcionales de mejora:

### Utilizar alguna librería que facilite el mantenimiento de los scripts DDL de base de datos.
	He utilizado flyway para la gestión de scripts, lo utilizo para cargar los datos en la bd h2 al iniciar la aplicación

### Documentación de la API.
	Se ha utilizado openapi v2 que lleva la integración con swagger. Para acceder : http://localhost:8080/swagger-ui/index.html

### Implementar algún consumer/producer para algún broker (Rabbit, Kafka, etc).
	Se ha implementado un listener de Kafka que se utiliza para crear naves. Símplemente comprueba que la nave sea válida. Está apagado por defecto

### Presentarla con Docker.
	He dejado creado el dockerfile para la aplicación.